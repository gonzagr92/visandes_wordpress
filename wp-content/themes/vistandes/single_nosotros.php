<?php
/*
Template Name: Single Nosotros
*/
?>
<?php get_header(); ?>
<!-- - provide(:current_page, t('quienes_somos.titulo')) -->
<section id="quienes" class="section_custom">
  <div class="container">
    <div class="title">
      <h3><?php the_field('nosotros_titulo'); ?></h3>
    </div>
    <div class="resume col-md-8 col-md-offset-2">
      <p><?php the_field('nosotros_p1'); ?></p>
      <p><?php the_field('nosotros_p2'); ?></p>
      <p><?php the_field('nosotros_p3'); ?></p>
      <p><?php the_field('nosotros_p4'); ?></p>
      <p><?php the_field('nosotros_p5'); ?></p>
		</div>
	</div>
</section>
<?php get_footer(); ?>
