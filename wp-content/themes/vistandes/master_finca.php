<?php
/*
Template Name: Master Finca
*/
?>
<?php get_header(); ?>
<!--
- provide(:current_page, t('fincas.titulo'))
-->
<?php
	$fincas = get_children(array(
    'post_parent' => $post->ID,
    'post_type' => 'page',
    'post_status' => 'publish',
	));
?>
<section id="fincas" class="section_custom">
  <div class="container">
    <div class="title">
      <h3><?php the_field('finca_master_titulo'); ?></h3>
		</div>
    <p class="col-md-offset-1 col-md-10">
      <i><?php the_field('finca_master_descripcion'); ?></i>
		</p>
    <div class="clearfix"></div>
    <div class="tab-header">
      <ul>
				<?php $i=0 ?>
				<?php foreach ($fincas as $post): ?>
					<?php setup_postdata( $post ) ?>
					<li>
            <a class="<?php if($i == 0){echo "active";}?>" href="#tab-<?php echo $post->post_name ?>">
							<?php the_title(); ?>
						</a>
					</li>
					<?php $i++; ?>
				<?php endforeach;	?>
				<?php wp_reset_postdata(); ?>
			</ul>
		</div>
	</div>
	<?php $i=0; ?>
	<?php foreach ($fincas as $post): ?>
		<?php setup_postdata( $post ) ?>
    <div class="tab <?php if($i != 0){echo "hidden";}?>" id="tab-<?php echo $post->post_name ?>">
      <div class="description col-md-offset-1 col-md-10">
        <p><?php the_field('finca_descripcion'); ?></p>
			</div>
      <div class="galeria col_full clearfix container-fluid">
        <div class="masonry-thumbs col-5" data-big="2" data-lightbox="gallery">
          <a href="<?php the_field('img_1'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_1'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_2'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_2'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_3'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_3'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_4'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_4'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_5'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_5'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_6'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_6'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_7'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_7'); ?>" class="image_fade"/></a>
				</div>
			</div>
		</div>
		<?php $i++; ?>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	</div>
</section>
<script type="text/javascript">
	$(function(){
    $('.tab-header a').click(function(event){
      $('.tab-header .active').removeClass('active');
      $(this).addClass('active');

      $('.tab').addClass('hidden');
      $($(this).attr('href')).removeClass('hidden');

      SEMICOLON.widget.masonryThumbs();
      event.preventDefault();
    });
  });
</script>

<?php get_footer(); ?>
