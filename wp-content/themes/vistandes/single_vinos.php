<?php
/*
Template Name: Single Vinos
*/
?>
<?php get_header(); ?>
<!-- - provide(:current_page, 'vinos_single') -->
<!-- - provide(:current_category, @wines.title) -->

<?php $wines = get_posts(array( 'category_name' => strtolower(get_field('categoria_titulo')))); ?>
<?php $categoria_titulo = get_field( "categoria_titulo" ); ?>

<div id="wine_single" class="section_custom">
  <div class="container">
    <div class="title">
      <h3><?php the_field("categoria_titulo"); ?></h3>
		</div>
    <div class="col-lg-8 col-lg-offset-1 col-md-12">
      <p><?php the_field("categoria_descripcion"); ?></p>
		</div>
    <div class="clearfix"></div>
    <div class="wine-detail">
      <div class="list-vinos col-sm-12 col-md-3">
        <ul>
					<?php $i = 0; ?>
					<?php foreach ($wines as $post): ?>
						<?php setup_postdata( $post ) ?>
						<li class="<?php if ($i == 0){ echo "active";}?>">
              <a href="#<?php echo str_replace(' ','_', the_field("vino_titulo"));?>">
               <h4><?php the_field("vino_titulo"); ?></h4>
						 </a>
						</li>
						<?php $i++; ?>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</ul>
			</div>
			<div class="clearfix"></div>
      <?php $i = 0; ?>
			<?php foreach ($wines as $post): ?>
				<?php setup_postdata( $post ) ?>
        <div id="<?php echo str_replace(' ','_', the_field("vino_titulo"));?>" class="flex" style="<?php if($i != 0){echo "display: none !important;";}?>">
          <div class="col-wine-img col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-3 col-md-offset-0">
            <img src="<?php the_field('vino_imagen');?>" />
					</div>
          <div class="col-wine-description col-sm-12 col-md-6 col-xl-4">
            <div class="title-wine">
              <h4><i><?php echo $categoria_titulo; ?>, <?php the_field("vino_titulo")?></i></h4>
						</div>
						<div class="row row-image">
              <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-image">
                <div class="icon-1"></div>
							</div>
              <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-border">
                <div class="terroir-wine">
                  <div class="text-container">
										<?php the_field('vino_terroir'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row row-image">
              <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-image">
                <div class="icon-2"></div>
							</div>
              <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-border">
                <div class="taste-wine">
                  <div class="text-container">
										<?php the_field('vino_taste'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row row-image">
              <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-image">
                <div class="icon-3"></div>
							</div>
              <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-border">
                <div class="recommendation-wine">
                  <div class="text-container">
										<?php the_field('vino_recomendacion') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $i++; ?>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>

  <script type="text/javascript">
    $(function(){
      $('.list-vinos a, .list-vinos-mobile a').click(function(event){
        var w_href = $($(this).attr('href'));

        $('.flex').attr('style', 'display:none !important');

        $(w_href).css("display", "block").fadeIn(2500);

        $('.list-vinos li, .list-vinos-mobile li').removeClass('active');
        $(this).parent().addClass('active');

        event.preventDefault();
      });
    });
	</script>
</div>

<?php get_footer(); ?>
