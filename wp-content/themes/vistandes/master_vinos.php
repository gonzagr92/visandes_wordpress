<?php
/*
Template Name: Master Vinos
*/
?>
<?php get_header(); ?>
<!-- - provide(:current_page, t('vinos.titulo')) -->
<?php
	$categorias = get_children(array(
    'post_parent' => $post->ID,
    'post_type' => 'page',
    'post_status' => 'publish',
		'orderby' => 'meta_value_num',
		'order' => 'ASC'
	));
?>
<div id="wine" class="section_custom">
  <div class="container">
    <div class="title">
      <h3> <?php the_field('vinos_titulo'); ?></h3>
		</div>
    <div class="col-lg-8 col-lg-offset-2 col-md-12">
      <p> <?php the_field('vinos_p1'); ?></p>
      <p> <?php the_field('vinos_p2'); ?></p>
      <p> <?php the_field('vinos_p3'); ?></p>
		</div>
    <div class="clearfix"></div>
    <div class="img-container full-container">
			<?php foreach ($categorias as $post): ?>
				<?php setup_postdata( $post ) ?>
				<div class="col-md-4 col-sm-12 container-img-wine">
          <a href="<?php the_permalink(); ?>">
            <img src="<?php the_field("categoria_imagen"); ?>" />
            <!--h2><?php the_field("categoria_titulo"); ?></h2-->
					</a>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
		<div class="img-container mobile-container">
      <div class="bxslider">
        <?php foreach ($categorias as $post): ?>
					<?php setup_postdata( $post ) ?>
          <div class="slide">
            <a href="<?php the_permalink(); ?>">
              <img src="<?php the_field("categoria_imagen"); ?>" />
              <!--h2><?php the_field("categoria_titulo"); ?></h2-->
						</a>
					</div>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
	    $('.bxslider').bxSlider({
	      pager: false,
	      slideWidth: 200,
	      maxSlides: 1,
	      moveSlides: 1
	    });
	  });
	</script>
</div>
<?php get_footer(); ?>
