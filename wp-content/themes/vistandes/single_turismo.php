<?php
/*
Template Name: Single Turismo
*/
?>
<?php get_header(); ?>
<!--
- provide(:current_page, t('turismo.titulo'))
-->
<section id="fincas" class="section_custom eventos">
  <div class="container">
    <div class="title">
      <h3><?php the_field('turismo_titulo'); ?></h3>
    </div>
    <p><?php the_field('turismo_descripcion'); ?></p>
    <div class="tab-header">
      <ul>
        <li>
          <a class="active" href="#tab1"><?php the_field('turismo_tour_titulo'); ?></a>
        </li>
        <li>
          <a href="#tab2"><?php the_field('turismo_visitas_titulo'); ?></a>
        </li>
        <li>
          <a href="#tab3"><?php the_field('turismo_eventos_titulo'); ?></a>
        </li>
      </ul>
    </div>
    <div id="tab1" class="tab">
      <div class="description col-md-offset-1 col-md-10">
        <p><?php the_field('turismo_tour_descripcion'); ?></p>
        <div class="fslider flex-thumb-grid grid-6.bottommargin-sm" data-arrows="false" data-animation="fade" data-thumbs="true">
          <div class="flexslider">
            <div class="slider-wrap">
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img1_min'); ?>">
                <img src="<?php the_field('turismo_tour_img1'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img2_min'); ?>">
                <img src="<?php the_field('turismo_tour_img2'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img3_min'); ?>">
                <img src="<?php the_field('turismo_tour_img3'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img4_min'); ?>">
                <img src="<?php the_field('turismo_tour_img4'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img5_min'); ?>">
                <img src="<?php the_field('turismo_tour_img5'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_tour_img6_min'); ?>">
                <img src="<?php the_field('turismo_tour_img6'); ?>"/>
              </div>
            </div>
          </div>
        </div>
        <p><?php the_field('turismo_horario_titulo'); ?></p>
        <p><?php the_field('turismo_horario_detalle'); ?></p>
      </div>
    </div>
    <div id="tab2" class="tab hidden">
      <div class="description col-md-offset-1 col-md-10">
        <p><?php the_field('turismo_visitas_descripcion'); ?></p>
        <div class="fslider flex-thumb-grid grid-6 bottommargin-sm" data-arrows="false" data-animation="fade" data-thumbs="true">
          <div class="flexslider">
            <div class="slider-wrap">
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img1_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img1'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img2_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img2'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img3_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img3'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img4_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img4'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img5_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img5'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_visitas_img6_min'); ?>">
                <img src="<?php the_field('turismo_visitas_img6'); ?>"/>
              </div>
            </div>
          </div>
        </div>
        <p><?php the_field('turismo_horario_titulo'); ?></p>
        <p><?php the_field('turismo_horario_detalle'); ?></p>
      </div>
    </div>
    <div id="tab3" class="tab hidden">
      <div class="description col-md-offset-1 col-md-10">
        <p><?php the_field('turismo_eventos_descripcion'); ?></p>
        <div class="fslider flex-thumb-grid grid-6 bottommargin-sm" data-arrows="false" data-animation="fade" data-thumbs="true">
          <div class="flexslider">
            <div class="slider-wrap">
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img1_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img1'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img2_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img2'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img3_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img3'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img4_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img4'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img5_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img5'); ?>"/>
              </div>
              <div class="slide" data-thumb="<?php the_field('turismo_eventos_img6_min'); ?>">
                <img src="<?php the_field('turismo_eventos_img6'); ?>"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-10.col-md-offset-1.btn-container">
      <a class="btn" href=contacto_url><?php the_field('turismo_texto_contacto'); ?></a>
    </div>
  </div>
  <script type="text/javascript">
    $(function(){
      $('.tab-header a').click(function(event){
        $('.tab-header .active').removeClass('active');
        $(this).addClass('active');

        $('.tab').addClass('hidden');
        $($(this).attr('href')).removeClass('hidden');

        SEMICOLON.widget.masonryThumbs();
        event.preventDefault();
      });
    });
  </script>
</section>
<?php get_footer(); ?>
