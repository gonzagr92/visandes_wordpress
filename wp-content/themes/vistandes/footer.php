	</div> <!-- Fin de wrapper -->
	<?php wp_footer(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/functions.js"></script>
  <script type="text/javascript">
    $(function(){

      var current_page = "#{yield(:current_page)}".toUpperCase().trim();
      var current_category = "#{yield(:current_category)}";
      var current_finca = "#{yield(:current_finca)}"

      switch(current_page) {
        case "VINOS_SINGLE":
          $('.subclass_wines>ul').css('display', 'block');
          $('#primary-menu .subclass_wines div').each(function(){
            if($(this).html().trim() === current_category){
              $(this).parent().parent().addClass('current');
              return false;
            }
          });
          break;
        case "FINCA_SINGLE":
          $('.subclass_fincas>ul').css('display', 'block');
          $('#primary-menu .subclass_fincas div').each(function(){
            if($(this).html().trim() === current_finca){
              $(this).parent().parent().addClass('current');
              return false;
            }
          });
          break;
        default:
          $('#primary-menu div').each(function(){
            var st = $(this).html().replace('&nbsp;' , " ").toUpperCase().trim();
            if(st === current_page){
              $(this).parent().parent().addClass('current');
              return false;
            }
          });
      }

      function getText($node){
        return $node.toString();
      }
    });
  </script>
  <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-79231601-1', 'auto');
    ga('send', 'pageview');
  </script>
</body>
</html>
