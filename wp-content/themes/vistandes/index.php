<?php get_header(); ?>

<section id="slider" class="slider-parallax swiper_wrapper full-screen force-full-screen clearfix">
  <div class="swiper-container swiper-parent">
    <div class="swiper-wrapper">
      <div class="swiper-slide dark" style="background-image: url('<?php bloginfo('template_url'); ?>/images/inicio_1.jpg');"></div>
      <div class="swiper-slide" style="background-image: url('<?php bloginfo('template_url'); ?>/images/inicio_2.jpg'); background-position: center top;"></div>
      <div class="swiper-slide dark" style="background-image: url('<?php bloginfo('template_url'); ?>/images/inicio_3.jpg');"></div>
    </div>
    <div id="slider-arrow-left">
      <i class="icon-angle-left"></i>
    </div>
    <div id="slider-arrow-right">
      <i class="icon-angle-right"></i>
    </div>
  </div>
</section>
<section id="edad">
  <div class="container">
    <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Vistandes Logo"/>
    <h3 class="text"> Al ingresar a nuestro sitio, <br/> Usted declara tener la edad legal para consumir bebidas alcohólicas. <br/> <i>(By entering our site you declare to have the legal age to consume alcoholic drinks.)</i></h3>
    <button id="close_edad">
      <h4> SOY MAYOR DE EDAD <br/> <i>(I HAVE THE LEGAL AGE)</i></h4>
    </button>
  </div>
</section>

<script type="text/javascript">
  $(function(){
    $.fn.extend({
      animateCss: function (animationName, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
          $(this).removeClass('animated ' + animationName);
          typeof callback === 'function' && callback();
        });
      }
    });
    $('#close_edad').click(function() {
      $('#edad').animateCss("fadeOutUpBig", function(){ $('#edad').remove(); });
    });
    var swiperSlider = new Swiper('.swiper-parent',{
    paginationClickable: false,
      slidesPerView: 1,
      grabCursor: true,
      autoplay: 3000,
      speed: 650,
      loop: true,
      onSwiperCreated: function(swiper){
        $('[data-caption-animate]').each(function(){
          var $toAnimateElement = $(this);
          var toAnimateDelay = $(this).attr('data-caption-delay');
          var toAnimateDelayTime = 0;
          if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 750; } else { toAnimateDelayTime = 750; }
          if( !$toAnimateElement.hasClass('animated') ) {
            $toAnimateElement.addClass('not-animated');
            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
            setTimeout(function() {
              $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
            }, toAnimateDelayTime);
          }
        });
      },
      onSlideChangeStart: function(swiper){
        $('#slide-number-current').html(swiper.activeIndex + 1);
        $('[data-caption-animate]').each(function(){
          var $toAnimateElement = $(this);
          var elementAnimation = $toAnimateElement.attr('data-caption-animate');
          $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
        });
      },
      onSlideChangeEnd: function(swiper){
        $('#slider .swiper-slide').each(function(){
          if($(this).find('video').length > 0) { $(this).find('video').get(0).pause(); }
        });
        $('#slider .swiper-slide:not(".swiper-slide-active")').each(function(){
          if($(this).find('video').length > 0) {
            if($(this).find('video').get(0).currentTime != 0 ) $(this).find('video').get(0).currentTime = 0;
          }
        });
        if( $('#slider .swiper-slide.swiper-slide-active').find('video').length > 0 ) { $('#slider .swiper-slide.swiper-slide-active').find('video').get(0).play(); }

        $('#slider .swiper-slide.swiper-slide-active [data-caption-animate]').each(function(){
          var $toAnimateElement = $(this);
          var toAnimateDelay = $(this).attr('data-caption-delay');
          var toAnimateDelayTime = 0;
          if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 300; } else { toAnimateDelayTime = 300; }
          if( !$toAnimateElement.hasClass('animated') ) {
            $toAnimateElement.addClass('not-animated');
            var elementAnimation = $toAnimateElement.attr('data-caption-animate');
            setTimeout(function() {
              $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
            }, toAnimateDelayTime);
          }
        });
      }
    });

    $('#slider-arrow-left').on('click', function(e){
      e.preventDefault();
      swiperSlider.swipePrev();
    });

    $('#slider-arrow-right').on('click', function(e){
      e.preventDefault();
      swiperSlider.swipeNext();
    });

    $('#slide-number-current').html(swiperSlider.activeIndex + 1);
  });
</script>

<?php get_footer(); ?>
