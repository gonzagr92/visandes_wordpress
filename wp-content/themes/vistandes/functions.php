<?php
if ( ! current_user_can( 'manage_options' ) ) {
	show_admin_bar( false );
} elseif (current_user_can( 'manage_options' )) {
	show_admin_bar( true );
	function on_style(){
		echo '<script type="text/javascript">$(function(){$("nav").css("top","30px");});</script>';
	}
	add_action('wp_footer', 'on_style');
}

// Registro del menú de WordPress

add_theme_support( 'nav-menus' );
if ( function_exists( 'register_nav_menus' ) ) {
  register_nav_menus(
    array(
      'main' => 'Main'
    )
  );
}

function include_output($filename){
		ob_start();
		include $filename;
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
}

function enviarMensaje(){

	$GLOBALS['data'] = $_POST;

	$msg = include_output('contact_mailer.php');
	$to  = 'gonza.gr92@gmail.com';
	//'info@vistandes.com, administracion@vistandes.com';
	$subject = $_POST['asunto'];

	$headers = 'MIME-Version: 1.0'."\r\n";
	$headers .= 'Content-type:text/html;charset=UTF-8'."\r\n";
	$headers .= 'From: Vistandes Web <web@vistandes.com>' . "\r\n";

	// Enviarlo
	try {
			mail($to, $subject, $msg, $headers);
			return json_encode(array('respuesta' => 'ok'));
	} catch (Exception $exc) {
			echo 'Se produjo un error';
	}
	die();
}

add_action('wp_ajax_contact_email', 'enviarMensaje');
add_action('wp_ajax_nopriv_contact_email', 'enviarMensaje');
