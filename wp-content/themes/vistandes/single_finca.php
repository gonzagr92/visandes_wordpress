<?php
/*
Template Name: Single Finca
*/
?>
<?php get_header(); ?>
<!--
- provide(:current_page, 'finca_single')
- provide(:current_finca, @finca.name)
-->
<section id="fincas" class="section_custom">
  <div class="container">
    <div class="title">
			<h3><?php the_field('finca_nombre'); ?></h3>
		</div>
    <div class="tab">
      <div class="description col-md-offset-1 col-md-10">
        <p><?php the_field('finca_descripcion'); ?></p>
			</div>
      <div class="galeria col_full clearfix">
        <div class="masonry-thumbs col-5" data-big="2" data-lightbox="gallery">
          <a href="<?php the_field('img_1'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_1'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_2'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_2'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_3'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_3'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_4'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_4'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_5'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_5'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_6'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_6'); ?>" class="image_fade"/></a>
					<a href="<?php the_field('img_7'); ?>" data-lightbox="gallery-item"><img src="<?php the_field('img_min_7'); ?>" class="image_fade"/></a>
				</div>
			</div>
		</div>
		<div class="end_quote">
      <blockquote>
        <p><i>= t('fincas.descripcion')</i></p>
			</blockquote>
		</div>
	</div>
</section>
<?php get_footer(); ?>
