<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
<head>
	<meta charset="UTF-8">
  <title>Vistandes • <?php the_title(); ?></title>
  <!-- Definir viewport para dispositivos web móviles -->
  <meta name="viewport" content="width=device-width, minimum-scale=1">

	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"/>
	<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Quicksand"/>
	<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Tangerine"/>
	<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.js" ></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bx-slider.js" ></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/toastr.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.animateCss.js" ></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/plugins.js" ></script>

</head>
<body class="stretched side-header">
  <div id="wrapper" class="clearfix">
		<?php get_sidebar(); ?>
