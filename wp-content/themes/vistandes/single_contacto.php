<?php
/*
Template Name: Single Contacto
*/
?>
<?php get_header(); ?>
<!-- - provide(:current_page, t('contacto.titulo'))-->

<section id="content">
  <div class="content-wrap">
    <div class="container clearfix">
      <div class="col_half">
        <div class="fancy-title title-dotted-border">
          <h3> <?php the_field('contacto_subtitulo'); ?> </h3>
        </div>
        <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> <?php the_field('contacto_mensaje_enviado'); ?>"></div>
        <form name="form-contact" id="template-contact" class="form nobottommargin_for" method="POST" action="/wp-admin/admin-ajax.php">
          <!-- :contact_form, url: contact_mail_url,, html: {:"} do |f| -->
          <div class="form-process"></div>
          <div class="col_half">
            <label for="nombre">
              <?php the_field('contacto_nombre'); ?>
              <small> * </small>
            </label>
            <input type="text" class="sm-form-control required" required="required" aria-required="true" maxlength="50" name="nombre" id="nombre"/>
          </div>
          <div class="col_half col_last">
            <label for="email"> Email <small> * </small></label>
            <input type="text" class="required email sm-form-control" required="required" aria-required="true" maxlength="255" name="email" id="email"/>
          </div>
          <div class="clear"></div>
          <div class="col_full">
            <label for="asunto">
              <?php the_field('contacto_asunto'); ?>
              <small> * </small>
            </label>
            <input type="text" class="required sm-form-control" required="required" aria-required="true" maxlength="50" name="asunto" id="asunto"/>
          </div>
          <div class="clear"></div>
          <div class="col_full">
            <label for="template-contactform-message">
              <?php the_field('contacto_mensaje'); ?>
              <small> * </small>
            </label>
            <textarea class="required sm-form-control" required="required" aria-required="true" rows="6" cols="30" maxlength="400" name="mensaje" id="mensaje"></textarea>
          </div>
          <div class="col_full">
						<input type="submit" value="<?php the_field('contacto_enviar'); ?>" id="submit-button" class="button button-3d nomargin" tabindex="5"/>
          </div>
          <input type="hidden" name="action" value="contact_email">
        </form>
      </div>
      <div class="col_half col_last">
        <section id="google-map" class="gmap" style="height: 410px;"></section>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOMwzO67ohjT5ZdENZvRbvz_xiehstMO4&callback=initMap"></script>
        <script type="text/javascript">
          $(function() {
            var map;
            // Creando Latitud y longitud del mapa de Mendoza
            var latlng = new google.maps.LatLng(-33.0242274, -68.7589029);

            // Se establecen las corrdenadas del mapa
            // Opciones del mapa
            var options = {
                //     Centrando el mapa
                center: latlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Creando el mapa referenciando el div
            map = new google.maps.Map($('#google-map')[0], options);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(-33.0242274, -68.7589029),
                map: map,
                animation: google.maps.Animation.DROP
            });

          });
        </script>
      </div>
      <div class="clear"></div>
      <div class="row clear-bottommargin">
        <div class="col-md-3 col-sm-6 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a target="_blank" href="https://www.google.com.ar/maps/place/Vistandes/@-33.024256,-68.758978,15z/data=!4m2!3m1!1s0x0:0xd6bbd8c3d6f103bc?sa=X&ved=0ahUKEwi0--yh3LvMAhVDS5AKHTdEDFYQ_BIIfzAN">
                <i class="icon-map-marker2"></i>
              </a>
            </div>
            <h3>
              <?php the_field('contacto_direccion'); ?>
              <span class="subtitle"> Urquiza 5517, Maipú, Mendoza <br/> Argentina</span>
            </h3>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a href="#">
                <i class="icon-phone3"></i>
              </a>
            </div>
            <h3>
              <?php the_field('contacto_telefonos'); ?>
              <span class="subtitle"> +54 261 5246213  <br/> +54 261 5246214  <br/> +54 9 261 2189752 <br/>+54 9 261 4542847</span>
            </h3>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a target="_blank" href="mailto:info@vistandes.com?Subject=Contacto">
                <i class="icon-mail"></i>
              </a>
            </div>
            <h3>
              <?php the_field('contacto_correos'); ?>
              <span class="subtitle">
                <a target="_blank" href="mailto:info@vistandes.com?Subject=Contacto"> info@vistandes.com</a>
                <a target="_blank" href="mailto:administracion@vistandes.com?Subject=Contacto"> administracion@vistandes.com</a>
              </span>
            </h3>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a target="_blank" href="https://www.facebook.com/Vistandes">
                <i class="icon-facebook"></i>
              </a>
            </div>
            <h3>
              <?php the_field('contacto_siguenos'); ?>
              <span class="subtitle">
                <a target="_blank" href="https://www.facebook.com/Vistandes"> facebook.com/Vistandes</a>
              </span>
            </h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(function(){
    $('#template-contact').submit(function(e){
      e.preventDefault();
      $('.form-process').fadeIn();
      var formData = new FormData(this);
      $.ajax({
        type: "POST",
        dataType: "JSON",
        url: "/wp-admin/admin-ajax.php",
        data: formData,
        contentType: false,
        processData: false,
        success: function(data){
          $('.form-process').fadeOut();
          $('#template-contact').find('.sm-form-control').val('');
          SEMICOLON.widget.notifications($('#contact-form-result'));
        },
        error: function(msg){

        }
      });

      return false;
    });
  });
</script>

<?php get_footer(); ?>
