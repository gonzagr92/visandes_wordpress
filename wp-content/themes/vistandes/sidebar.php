
<header class="no-sticky" id="header">
	<div id="header-wrap">
		<div class="nobottomborder" id="logo">
			<a class="standard-logo" data-dark-logo="<?php bloginfo('template_url'); ?>/images/logo.png" href="/">
				<img alt="Vistandes Logo" src="<?php bloginfo('template_url'); ?>/images/logo.png">
			</a>
			<a class="retina-logo" data-dark-logo="<?php bloginfo('template_url'); ?>/images/logo.png" href="/">
				<img alt="Vistandes Logo" src="<?php bloginfo('template_url'); ?>/images/logo.png">
			</a>
		</div>
		<div class="container clearfix">
			<div id="primary-menu-trigger">
				<i class="icon-reorder"></i>
			</div>

			<?php wp_nav_menu(array('menu' => 'Main', 'container' => 'nav')); ?>

			<div style="display:flex">
				<div class="clearfix visible-md visible-lg" style="margin: auto">
					<a class="social-icon si-small si-borderless si-facebook" href="https://www.facebook.com/Vistandes/?ref=br_rs&amp;fref=nf" target="_blank"><i class="icon-facebook"></i><i class="icon-facebook"></i></a><a class="social-icon si-small si-borderless si-gplus"
						href="https://www.google.com.ar/maps/place/Vistandes/@-33.024256,-68.758978,15z/data=!4m5!3m4!1s0x0:0xd6bbd8c3d6f103bc!8m2!3d-33.024256!4d-68.758978" target="_blank"><i class="icon-map"></i><i class="icon-map"></i></a><a class="social-icon si-small si-borderless si-twitter"
						href="mailto:info@vistandes.com?Subject=Contacto" target="_blank"><i class="icon-email2"></i><i class="icon-email2"></i></a>
					<br>
					<br>
					<div class="lang">
						<a href="<?php the_permalink()?>?lang=en">
							<img alt="Language Flag" src="<?php bloginfo('template_url'); ?>/images/english.png" alt="English" title="View this site in english">
						</a>
						<a href="<?php the_permalink()?>?lang=es">
							<img alt="Language Flag" src="<?php bloginfo('template_url'); ?>/images/spanish.png" alt="Spanish" title="Ver en español">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<script type="text/javascript">
	$(function(){
		var current_page = "<?php the_permalink(); ?>";

		$('.menu').attr('id', 'primary-menu');
		$('.menu > ul').append('<li class="mobile"><div class="lang">' + $('.lang').html() + '</div></li>');

		$('.menu li').each(function(){
			if ($(this).find('a').attr('href') === current_page){
				if($(this).parent().hasClass('children')){
					$(this).parent().parent().addClass('current');
				}else{
					$(this).addClass('current');
				}
			}
		});
	});
</script>
